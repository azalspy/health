from azalspy.health.helpers import *

###################################################################################################

class HealthDrug(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

    info = scrapy.Field()
    code = scrapy.Field()
    mole = scrapy.Field()

    brand = scrapy.Field()
    famil = scrapy.Field()
    maker = scrapy.Field()

    type = scrapy.Field()
    pack = scrapy.Field()
    cons = scrapy.Field()
    ages = scrapy.Field()

    etat = scrapy.Field()
    cnss = scrapy.Field()
    sale = scrapy.Field()
    pcep = scrapy.Field()
    tier = scrapy.Field()
    gros = scrapy.Field()

    prix_c = scrapy.Field()
    prix_v = scrapy.Field()
    prix_h = scrapy.Field()
    prix_r = scrapy.Field()

    attr = scrapy.Field()

    SQL_TABLE = 'health_drugs'

###################################################################################################


