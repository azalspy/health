#-*- coding: utf-8 -*-

from azalspy.health.utils import *

####################################################################################

class MedicamentMaSpider(BaseSpider):
    name = "medicament.ma"
    start_urls = [
        'https://medicament.ma/listing-des-medicaments/',
    ]

    def parse(self, response):
        for a in response.xpath('//div[@class="btn-group"]/a'):
            yield response.follow(a, callback=self.parse_list)

    def parse_list(self, response):
        for link in response.xpath('//table//a/@href').extract():
            yield scrapy.Request(link, callback=self.parse_drug)

        for link in response.xpath('//ul[@class="pagination"]/li[@class=""]//a/@href').extract():
            yield scrapy.Request(link, callback=self.parse_list)

    mapper = {
        "Indications": "info",
        "Code ATC": "code",
        "Composition": "mole",

        "Famille": "brand",
        "Tableau": "famil",
        "Distributeur ou fabriquant": "maker",

        "Nature du Produit": "type",
        u'Pr\xe9sentation': "pack",
        "Conservation": "cons",
        "Age minimal d'utilisation": "ages",

        "PPC": "prix_c",
        "PPV": "prix_v",
        "Prix hospitalier": "prix_h",
        "Base de remboursement / PPV": "prix_r",

        "Statut": "etat",
        "Remboursement": "cnss",
        "Princeps": "pcep",
        "Tiers Payant": "tier",
        "Grossesse": "gros",
    }

    def parse_drug(self, response):
        unit = response.css("div.single")

        item = HealthDrug()

        item["link"] = response.url
        item["name"] = "".join(unit.xpath(".//h3/text()").extract())

        #item["attr"] = "".join(response.xpath("//p[@class='attrgroup']//text()").extract())

        item["attr"] = {}

        for info in response.css("table.table-details tr"):
            f = "".join(info.xpath('./td[@class="field"]//text()').extract()).strip()
            v = "".join(info.xpath('./td[@class="value"]//text()').extract()).strip()

            if f in self.mapper:
                k = self.mapper[f]

                if k.startswith('prix'):
                    v = float(v.replace('dhs','').strip())

                item[k] = v
            else:
                item["attr"][f] = v

        if len(item['attr']) or True:
            yield item
